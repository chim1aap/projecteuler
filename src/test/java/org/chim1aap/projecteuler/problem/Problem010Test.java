package org.chim1aap.projecteuler.problem;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Test for problem 10.
 */
class Problem010Test {
	/**
	 * Test the solution of problem 10.
	 */
	@Test
	void test() {
		long expected = 142913828922L;
		long actual = new Problem010().solve();
		Assertions.assertEquals(expected, actual);
	}
}
