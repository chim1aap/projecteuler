package org.chim1aap.projecteuler.problem;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Problem 2.
 */
class Problem002Test {

	/**
	 * Test the solution of the problem.
	 */
	@Test
	void solutionTest() {
		Problem002 p = new Problem002();
		int expected = 4613732;
		long actual = p.solve();
		Assertions.assertEquals(expected, actual);
	}

}
