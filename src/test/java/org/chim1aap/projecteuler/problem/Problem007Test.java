package org.chim1aap.projecteuler.problem;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests for Problem 7.
 */
class Problem007Test {
	/**
	 * Test the solution of the problem.
	 */
	@Test
	void testProblem7() {
		int answerToProblem7 = 104743;
		long actual = new Problem007().solve();
		Assertions.assertEquals(answerToProblem7, actual);
	}
}
