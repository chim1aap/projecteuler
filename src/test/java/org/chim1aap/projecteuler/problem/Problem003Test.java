package org.chim1aap.projecteuler.problem;

import java.math.BigInteger;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Tests for problem 3.
 */
public class Problem003Test {

	/**
	 * The Problem class.
	 */
	Problem003 p;

	@Before
	public void setup() {
		p = new Problem003();
	}

	/**
	 * Test the prime factorization given in the example.
	 */
	@Test
	public void testExample() {
		int input = 13195;
		BigInteger[] expected = { BigInteger.valueOf(5), BigInteger.valueOf(7), BigInteger.valueOf(13),
				BigInteger.valueOf(29) };
		BigInteger[] actual = p.primeFactorization(input);
		Assert.assertArrayEquals(expected, actual);
	}

	/**
	 * Test the prime factorisation asked for the problem.
	 */
	@Test
	public void testSolution() {
		// Arrange.
		BigInteger input = new BigInteger("600851475143");

		// Act.
		BigInteger[] actual = p.primeFactorization(input);

		// Assert.
		BigInteger[] expected = { BigInteger.valueOf(71), BigInteger.valueOf(839), BigInteger.valueOf(1471),
				BigInteger.valueOf(6857) };
		Assert.assertArrayEquals(expected, actual);
	}

	/**
	 * Test the solution of the problem.
	 */
	@Test
	public void testSolve() {
		int expected = 6857;
		long actual = p.solve();
		Assert.assertEquals(expected, actual);
	}
}
