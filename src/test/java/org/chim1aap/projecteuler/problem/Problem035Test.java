package org.chim1aap.projecteuler.problem;

import org.junit.Assert;
import org.junit.Test;

/**
 * Problem 35.
 */
public class Problem035Test {

	/**
	 * Test the solution of problem 35.
	 */
	@Test
	public void test() {
		int expected = 55;
		int actual = (int) new Problem035().solve();
		Assert.assertEquals(expected, actual);
	}
}
