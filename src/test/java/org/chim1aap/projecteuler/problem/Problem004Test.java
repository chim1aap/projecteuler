package org.chim1aap.projecteuler.problem;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for Problem 4.
 */
public class Problem004Test {

	/**
	 * Test the solution of the problem.
	 */
	@Test
	public void testSolution() {
		Assert.assertEquals(906609, new Problem004().solve());
	}
}
