package org.chim1aap.projecteuler.problem;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Class for testing Problem 1.
 */
class Problem001Test {

	/**
	 * Test the solution of the problem.
	 */
	@Test
	void testDefaultValues() {
		Problem001 p = new Problem001();
		int expected = 233168;
		long actual = p.solve();
		Assertions.assertEquals(expected, actual);
	}

	/**
	 * Test the example solution given in the problem.
	 */
	@Test
	void testExampleValues() {
		Problem001 p = new Problem001(10);
		int expected = 23;
		long actual = p.solve();
		Assertions.assertEquals(expected, actual);
	}
}
