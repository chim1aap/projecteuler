package org.chim1aap.projecteuler.utility;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Class for testing the Fibonacci generator class.
 */
class FibonacciGeneratorTest {

	FibonacciGenerator fg;

	@BeforeEach
	void setup() {
		fg = new FibonacciGenerator();
	}

	@Test
	void testFirstValue() {
		int expected = 1;
		int actual = fg.next();
		Assertions.assertEquals(expected, actual);
	}

	@Test
	void testSecondValue() {
		// Act.
		fg.next();
		int actual = fg.next();

		// Assert.
		int expected = 2;
		Assertions.assertEquals(expected, actual);
	}
}
