package org.chim1aap.projecteuler.utility;

import org.junit.Assert;
import org.junit.Test;

/**
 * Class for testing the Number Extension class.
 */
public class NumberExtensionTest {

	/**
	 * Test the rotation function.
	 */
	@Test
	public void test() {
		int expected = 719;
		int actual = NumberExtension.rotate(197);
		Assert.assertEquals(expected, actual);
	}
}
