package org.chim1aap.projecteuler.utility;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the Palindrome Number class.
 */
public class PalindromeNumberTests {
	public PalindromeNumberExtension pn;

	@Before
	public void setup() {
		pn = new PalindromeNumberExtension();
	}

	/**
	 * Test a number which is not palindrome.
	 */
	@Test
	public void notPalindrome() {
		Assert.assertFalse(pn.isPalindrome(1234));
	}

	/**
	 * Test a number which is palindrome.
	 */
	@Test
	public void itIsPalindrome() {
		Assert.assertTrue(pn.isPalindrome(9009));
	}

}
