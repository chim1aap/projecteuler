package org.chim1aap.projecteuler.utility;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Test class for the primes class.
 */
public class PrimesTest {
	public Primes p;

	@Before
	public void setup() {
		p = new Primes();
	}

	/**
	 * A non prime test.
	 */
	@Test
	public void testPrimes() {
		boolean actual = p.isPrime(9);
		Assert.assertFalse(actual);
	}

	/**
	 * Test the first result of the prime generator.
	 */
	@Test
	public void testFirst() {
		int expected = 2;
		int actual = p.next();
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Test the second result of the prime generator.
	 */
	@Test
	public void testSecond() {
		int expected = 3;
		int actual = p.next(2);
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Test the 100th result of the prime generator.
	 */
	@Test
	public void testHundred() {
		int expected = 541;
		int actual = p.next(100);
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Test the circular prime example given in Project Euler.
	 */
	@Test
	public void testCircularPrimeExample() {
		// Of problem 35
		Assert.assertTrue(p.isCircularPrime(197));
	}

	/**
	 * Test the example given in Problem 7.
	 */
	@Test
	public void testProblem7Example() {
		int expected = 13;
		int actual = p.next(6);
		Assert.assertEquals(expected, actual);
	}

	/**
	 * Test the solution given in problem 7.
	 */
	@Test
	public void testProblem7() {
		int answerToProblem7 = 104743;
		int actual = p.next(10001);
		Assert.assertEquals(answerToProblem7, actual);
	}
}
