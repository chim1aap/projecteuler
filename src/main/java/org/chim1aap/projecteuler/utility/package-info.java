/**
 * General Purpose class Containing interesting properties of certain Numbers.
 * Examples are Palindrome numbers or Primes.
 */
package org.chim1aap.projecteuler.utility;