package org.chim1aap.projecteuler.utility;

/**
 * Class for additional operations on an integer.
 */
public class NumberExtension {

	/**
	 * Returns the amount of digits in a number (base 10).
	 * @param n the number to count digits for.
	 * @return the amount of digits that n represents.
	 */
	public static int getDigits(int n) {
		return (int) Math.log10(n) + 1;
	}

	/**
	 *  Returns the rotated version of a given number.
	 *  For example: 197 -> 719
	 * @param n the number to rotate.
	 * @return the rotated version of n.
	 */
	public static int rotate(int n) {
		int movedDigit = n % 10;
		int shiftRight = (n - movedDigit) / 10;
		return (int) (movedDigit * Math.pow(10, (getDigits(n) - 1)) + shiftRight);
	}
}
