package org.chim1aap.projecteuler.utility;

/**
 * Palindromic numbers are the same when read backwards, such as 12345654321 or 121.
 */
public class PalindromeNumberExtension extends NumberExtension {
	/**
	 * Returns whether a given number is palindrome.
	 * @param n the number to check.
	 * @return whether n is palindrome.
	 */
	public boolean isPalindrome(int n) {
		int remainder;
		int sum = 0;
		int temp = n;
		while (n > 0) {
			remainder = n % 10;
			sum = (sum * 10) + remainder;
			n /= 10;
		}
		return (temp == sum);
	}
}
