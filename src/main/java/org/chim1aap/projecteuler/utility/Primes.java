package org.chim1aap.projecteuler.utility;

import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;

/**
 * Primes are only divisible by 1 and itself. They are quite famous. Thus, they
 * are a prime target for algorithm puzzles.
 */
public class Primes extends NumberExtension {
	/**
	 * The current prime.
	 */
	int currentPrime = 1;

	/**
	 * The logger for this class.
	 */
	public Logger logger;

	public Primes() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	/**
	 * Check whether `n` is prime.
	 * This is an implementation of the Sieve of Eratosthenes,
	 * so it is slow for large numbers.
	 * <a href="https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes">Wikipedia page.</a>
	 * @param n the number to check primality for.
	 * @return whether n is prime.
	 */
	public boolean isPrime(int n) {
		// We only need to check up to the square root of n.
		for (int i = 2; i <= Math.sqrt(n); i++) {
			if (n % i == 0) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Checks whether a number is a circular prime.
	 * The number, 197, is called a circular prime because all rotations of the
	 * digits: 197, 971, and 719, are themselves prime.
	 */
	public boolean isCircularPrime(int n) {

		// Check whether it is prime.
		if (!isPrime(n)) {
			return false;
		}
		int digits = getDigits(n);
		for (int i = 0; i < digits; i++) {
			n = rotate(n);
			logger.fine("Checking: " + n);
			if (!isPrime(n)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Returns the next prime after `steps` primes in the list of primes.
	 * @param steps The amount of primes to skip.
	 * @return The next prime after skipping `steps` primes.
	 */
	public int next(int steps) {
		for (int i = 0; i < steps; i++) {
			this.next();
		}
		return this.currentPrime;
	}

	/**
	 * Returns the next prime.
	 * @return the next prime.
	 */
	public int next() {
		logger.fine("Current Prime: " + currentPrime);
		currentPrime += 1;
		while (!isPrime(currentPrime)) {
			currentPrime += 1;
		}
		logger.fine("Next Prime: " + currentPrime);
		return currentPrime;
	}
}
