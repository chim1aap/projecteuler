
package org.chim1aap.projecteuler.utility;

import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;

/**
 * Generates Fibonacci numbers.
 */
public class FibonacciGenerator {

	/**
	 * 	The current number in the sequence.
	 */
	private int now = 0;

	/**
	 * 	The previous number in the sequence.
	 */
	private int previous = 1;

	/**
	 *  The (logger) of this class.
	 */
	public Logger logger;

	/**
	 * Creates a new instance of the FibonacciGenerator class.
	 */
	public FibonacciGenerator() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	/**
	 * Returns the next number in the Fibonacci sequence.
	 * @return the next number.
	 */
	public int next() {
		int tmp = now;
		now = now + previous;
		logger.fine(now + " = " + tmp + " + " + previous);
		previous = tmp;
		return now;
	}
}