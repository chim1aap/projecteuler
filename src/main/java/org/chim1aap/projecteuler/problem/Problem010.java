
package org.chim1aap.projecteuler.problem;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;
import org.chim1aap.projecteuler.utility.Primes;

/**
 * Problem 10.
 */
public class Problem010 extends Problem {
	@Override
	public String description(){
		return """
				The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
				 
				Find the sum of all the primes below two million.
			""";
	}

	/**
	 * The logger for this class.
	 */
	public Logger logger;

	public Problem010() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	public long solve() {
		// Set up.
		Primes p = new Primes();
		long sum = 0;
		int maximum = 2000000;
		long prime = p.next(); // Seed first prime.

		// Add until more than the maximum.
		while (prime < maximum) {
			logger.log(Level.FINE, "prime = {0}", prime);
			logger.log(Level.FINE, "sum = " + sum);
			sum += prime;
			prime = p.next();
		}

		// Return the solution.
		return sum;
	}
}
