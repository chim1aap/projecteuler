package org.chim1aap.projecteuler.problem;

import java.util.logging.*;

import org.chim1aap.projecteuler.CLI;

/**
 * Solves Problem 1.
 */

public class Problem001 extends Problem {
	@Override
	public String description(){
		return """
			If we list all the natural numbers below 10 that are multiples of 3 or 5, we
			get 3, 5, 6 and 9. The sum of these multiples is 23.
							 
			Find the sum of all the multiples of 3 or 5 below 1000.
		""";
	}

	/**
	 * The upper bound of the problem.
	 */
	public int n = 1000;

	/**
	 * The first divisor asked in the problem.
	 */
	public int firstDivisor = 3;

	/**
	 * The second divisor asked in the problem.
	 */
	public int secondDivisor = 5;
	public Logger logger;


	/**
	 * Creates a new instance of the Problem001 class.
	 * This has the default parameters of the problem.
	 */
	public Problem001() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	/**
	 * Creates a new instance of the Problem001 class.
	 *
	 */
	public Problem001(int n) {
		this.n = n;
		Logger.getLogger(CLI.class.getName());
	}

	/**
	 * @return the sum 1+2+...+(n-1)+n
	 */
	public int triangleNumberGenerator(int n) {
		// Since either n or n+1 is even, the result is always an integer.
		return (int) ((0.5) * (n + 1) * n);
	}

	public long solve() {
		logger.info("Solving problem 001");

		// Calculate the sum of the numbers divisible by the first divisor.
		int totalFirst = firstDivisor * triangleNumberGenerator((n - 1) / firstDivisor);
		logger.log(Level.FINE, "totalFirst = {0}", totalFirst);

		// Calculate the sum of the number divisible by the second divisor.
		int totalSecond = secondDivisor * triangleNumberGenerator((n - 1) / secondDivisor);
		logger.log(Level.FINE, "totalSecond = {0}", totalSecond);

		// Calculate the sum of the number divisible by the (first * second) divisor.
		int totalFirstTimesSecond = (firstDivisor * secondDivisor)
				* triangleNumberGenerator((n - 1) / (firstDivisor * secondDivisor));
		logger.log(Level.FINE, "totalFirstTimesSecond = {0}", totalFirstTimesSecond);

		// Return the solution.
		return totalFirst + totalSecond - totalFirstTimesSecond;
	}
}