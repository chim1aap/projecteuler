
package org.chim1aap.projecteuler.problem;

import org.chim1aap.projecteuler.utility.Primes;

/**
 * Problem 7.
 */
public class Problem007 extends Problem {

	public String description(){
		return """
				 By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see
				 that the 6th prime is 13.
				 
				 What is the 10 001st prime number?
				""";
	}
	public long solve() {
		return new Primes().next(10001);
	}
}
