
package org.chim1aap.projecteuler.problem;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;
import org.chim1aap.projecteuler.utility.Primes;

/**
 * Problem 35.
 */
public class Problem035 extends Problem {
	@Override
	public String description() {
		return """
				Circular Primes.
				
				The number, 197, is called a circular prime because all rotations of the
				digits: 197, 971, and 719, are themselves prime.
				
				There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71,
				73, 79, and 97.
				
				How many circular primes are there below one million?
			""";
	}

	/**
	 * The logger for this class.
	 */
	public Logger logger;

	public Problem035() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	public long solve() {

		// Set up.
		int limit = 2000000;
		int count = 0;
		Primes p = new Primes();

		// Count the circular primes found.
		for (int i = 2; i < limit; i++) {
			if (p.isCircularPrime(i)) {
				logger.log(Level.FINE, "Found Circular Prime: {0}", p);
				count++;
			}
		}

		// Return the solution.
		return count;
	}
}
