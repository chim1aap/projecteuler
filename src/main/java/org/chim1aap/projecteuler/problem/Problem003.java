package org.chim1aap.projecteuler.problem;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;
import org.chim1aap.projecteuler.utility.Primes;

/**
 * Problem 3.
 */
public class Problem003 extends Problem {

	@Override
	public String description(){
		return """
				The prime factors of 13195 are 5, 7, 13 and 29.
				 
				What is the largest prime factor of the number 600851475143 ?
				""";
	}

	/**
	 * The logger for this class.
	 */
	public Logger logger;

	public Problem003() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	/**
	 * Returns the prime factorization of n
	 * @param n the number to factor.
	 * @return a BigInteger list of prime factorization.
	 */
	public BigInteger[] primeFactorization(int n) {
		return primeFactorization(BigInteger.valueOf(n));
	}

	/**
	 * Calculates the primes which divide `n`.
	 * This is build with BigInteger because the problem asks
	 * for the prime factors of 600851475143 which is bigger than 2^64. The Prime
	 * class should work provided that the largest prime is less than 2^32, because
	 * the Primes class is in integers.
	 *
	 * @param n the integer to factor.
	 * @return an array of primes.
	 * Note, each entry is unique. For example, n=8 will return an array of length 1, namely the number 2.
	 */
	public BigInteger[] primeFactorization(BigInteger n) {
		// TODO: Move this to Primes class.
		// Set up.
		Primes primeGenerator = new Primes();
		ArrayList<BigInteger> primes = new ArrayList<>();
		BigInteger currentPrime = BigInteger.valueOf(2);

		// Construct the arraylist of primes.
		while (n.compareTo(BigInteger.ONE) != 0) {
			logger.log(Level.FINE, "currentPrime = {0}", currentPrime);
			logger.log(Level.FINE, "n mod currentprime = {0}", n.mod(currentPrime).toString());

			// Check if it is divisible.
			if (n.mod(currentPrime).equals(BigInteger.ZERO)) {
				// Then add
				logger.log(Level.FINE, "Is Divisor: {0}", currentPrime);
				primes.add(currentPrime);
				// Remove the factors until no longer divisible by that prime.
				while (n.mod(currentPrime).equals(BigInteger.ZERO)) {
					logger.log(Level.FINE, "Dividing {0} " + n + " by " + currentPrime);
					n = n.divide(currentPrime);
				}
			}

			// Get the next prime.
			currentPrime = BigInteger.valueOf(primeGenerator.next());
		}
		logger.fine("Testing " + n + " with prime " + currentPrime);

		// ArrayList<BigInteger> => BigInteger[]
		BigInteger[] result = new BigInteger[primes.size()];
		for (int i = 0; i < primes.size(); i++) {
			result[i] = primes.get(i);
		}

		// Return the result.
		return result;
	}

	@Override
	public long solve() {
		// Get list of primes.
		BigInteger[] listOfPrimes = primeFactorization(BigInteger.valueOf(600851475143L));
		BigInteger maximum = BigInteger.ZERO;

		// Search for the maximum.
		for (BigInteger p : listOfPrimes) {
			if (p.compareTo(maximum) > 0) {
				maximum = p;
			}
		}

		// Return the solution.
		return maximum.intValue();
	}
}
