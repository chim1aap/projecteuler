/**
 * This package contains classes for each problem of Project Euler which is
 * solved. The direct answer can usually be found in the test file.
 */
package org.chim1aap.projecteuler.problem;
