package org.chim1aap.projecteuler.problem;

/**
 * Abstract class representing a problem on project euler.
 */
public abstract class Problem {
	/**
	 * Solves the problem.
	 * @return The solution to the problem.
	 * The return type is long because some problems have answers which are bigger
	 * than 2^32
	 */
	public abstract long solve();

	/**
	 * Returns the description of the problem.
	 * @return the description of the problem.
	 */
	public abstract String description();

}
