
package org.chim1aap.projecteuler.problem;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.chim1aap.projecteuler.CLI;
import org.chim1aap.projecteuler.utility.PalindromeNumberExtension;

/**
 * Problem 4.
 */
public class Problem004 extends Problem {
	public String description(){
		return """
				A palindromic number reads the same both ways. The largest palindrome made
				from the product of two 2-digit numbers is 9009 = 91 × 99.
				
				Find the largest palindrome made from the product of two 3-digit numbers.
			""";
	}

	/**
	 * The logger for this class.
	 */
	public Logger logger;

	public Problem004() {
		this.logger = Logger.getLogger(CLI.class.getName());
	}

	public long solve() {
		// Set up.
		PalindromeNumberExtension pn = new PalindromeNumberExtension();
		int maximum = 0;

		// Check for each i and j combination.
		for (int i = 1000; i > 0; i--) {
			for (int j = 1000; j > i; j--) {
				logger.log(Level.FINE, " i = " + i + ", j = " + j + ", maximum = " + maximum);
				// Check whether it palindrome and bigger than the maximum.
				if (i * j > maximum && (pn.isPalindrome(i * j))) {
					maximum = i * j;
				}
			}
		}

		// Return the solution.
		return maximum;
	}
}
