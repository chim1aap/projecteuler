package org.chim1aap.projecteuler;

import java.util.Scanner;
import java.util.logging.*;

import org.chim1aap.projecteuler.problem.*;

/**
 * Runs the CLI interface, allowing the user to ask solutions to certain problems.
 */
public class CLI {

	/**
	 * Runs the CLI interface.
	 * @param args command line arguments.
	 */
	public void run(String[] args) {

		/*
		 * The number of the problem.
		 */
		int n = 0;

		/*
		 * The logger of this class.
		 */
		Logger logger = Logger.getLogger(CLI.class.getName());

		// Set up logging.
		Handler consoleHandler = new ConsoleHandler();
		logger.addHandler(consoleHandler);
		logger.info("logging");

		// Check if a problem was requested on the command line.
		if (args.length != 0) {
			try {
				n = Integer.parseInt(args[0]);
				logger.info("n = " + n);
			} catch (Exception e) {
				logger.warning(String.format("Command %s was not a number", args[0]));
				System.out.printf("Command %s was not a number", args[0]);
				n = 0;
			}
		}

		// If n==0, then the problem number was either not provided, or was invalid.
		// In both cases, we ask the user.
		if (n == 0) {
			System.out.println("The problems which currently can be solved are:");
			// TODO: Generate the list dynamically.
			System.out.println("1, 2, 3, 4, 7, 10, 35");
			System.out.println("Which problem do you need the solution for?: ");
			System.out.println("Please enter the answer now.");

			// Set up the scanner.
			Scanner sc = new Scanner(System.in);

			// Ask for input until a valid answer was given.
			while (n <= 0) {
				String input = sc.nextLine();
				try {
					n = Integer.parseInt(input);
					logger.info("n = " + n);
				} catch (NumberFormatException e) {
					System.out.println("That was not a number");
					n = 0;
				}
				if (n <= 0) {
					System.out.println("Please input an integer above zero.");
				}
			}

			// Ask the user whether he wants the way how it was solved.
			System.out.printf("Selected Problem %d\n", n);
			System.out.println("Do you want to see a bit how it is solved? (yes/no)");
			String input = sc.nextLine();

			// LogLevel Fine also shows the steps in solving the problem.
			if (input.contains("y")) {
				logger.setLevel(Level.FINE);
				consoleHandler.setLevel(Level.FINE);
				logger.fine("Set to FINE");
			}

			// Close the scanner.
			sc.close();
		}

		//TODO: Make this dynamic.

		// Run the solver of the problem.
		boolean unsolved = false;
		long solution = 0L;
		switch (n) {
			case 1 -> solution = new Problem001().solve();
			case 2 -> solution = new Problem002().solve();
			case 3 -> solution = new Problem003().solve();
			case 4 -> solution = new Problem004().solve();
			case 7 -> solution = new Problem007().solve();
			case 10 -> solution = new Problem010().solve();
			case 35 -> solution = new Problem035().solve();
			default -> unsolved = true;
		}

		// Return the solution of the problem.
		// They are checked by Project Euler.
		if (unsolved) {
			System.out.printf("The Problem %d is not yet solved.", n);
		} else {
			System.out.printf("The solution to Problem %d is %d.", n, solution);
		}
	}

}
