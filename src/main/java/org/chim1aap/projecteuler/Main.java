package org.chim1aap.projecteuler;

/**
 * Main class starting the program.
 */
public class Main {

	/**
	 * Starts the program.
	 * @param args command line arguments.
	 */
	public static void main(String[] args) {
		CLI m = new CLI();
		m.run(args);
	}
}
